
var Catalog = {

    path: '/ajax',
    folders: {},
    filesBox: null,
    fcountBox: null,

    getFiles: function(cid, fcount, path) {

        if (cid in this.folders) {
            this.showFiles(cid, fcount);
        } else {
            $.post(path, {cid: cid}, function(response) {
                if (response.success) {
                    Catalog.folders[cid] = response.files;
                } else {
                    Catalog.folders[cid] = [];
                }
                Catalog.showFiles(cid, fcount);

            }, "json");
        }
        return true;
    },

    showFiles: function(cid, fcount) {

        Catalog.fcountBox.text('');
        html = '<h4>Files doesn\'t exist.</h4>';

        if (this.folders[cid].length > 0) {

            html = '<div class=\"row\">\n';
            for (i = 0; i < this.folders[cid].length; i++) {
                 class_name = "box_" + ((i+1) % 2 ? 'odd' : 'even');
                 html += '<div class=\"'+class_name+'\">';
                 html += this.folders[cid][i].name+' (type: ' + this.folders[cid][i].ext + ' | size: ' + this.folders[cid][i].size + ' '+ this.folders[cid][i].suffix + ')';
                 html += '</div>\n';
            }
            html += '</div>';
            Catalog.fcountBox.text('files: '+fcount);
        }
        Catalog.filesBox.html(html);
    }
}

$(document).ready(function() {
    $('.tree-toggle').click(function () {
        $(this).parent().children('ul.tree').toggle(10);
    });
    Catalog.filesBox = $( ".files-box" );
    Catalog.fcountBox = $( "#fcount-box" );
});
