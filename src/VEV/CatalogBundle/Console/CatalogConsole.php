<?php

namespace VEV\CatalogBundle\Console;

// use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
// use Symfony\Bundle\FrameworkBundle\Console\Console;
// use Doctrine\ORM\EntityManager;
use VEV\CatalogBundle\Lib\Tree;
use VEV\CatalogBundle\Entity\Files;

class CatalogConsole
{
    private $tree = null; // Tree object
    private $doctrine = null;
    private $files = [];

    function __construct($doctrine = null) {

        if (!$doctrine) {
            echo "Doctrine not defined...";
            exit();
        }
        $this->doctrine = $doctrine; // for files
        $this->tree = new Tree($doctrine);
    }

    public function create($path = '') 
    {
        if (!$path) return false;

        $result = false;

        if (is_dir($path)) {

            list($result) = $this->recursiveCreating($path);

            if ($result) {

                if (count($this->files)) {
                    $em = $this->doctrine->getManager();
                    foreach ($this->files as $fileObj) {
                        $em->persist($fileObj);
                    }
                    $em->flush();
                }
                $result = true;
            }
        }

        return $result;
    }

    private function recursiveCreating($path, $subpath = '', $parentId = 0) 
    {
        if (is_file($path) && $parentId) {

            $fsize = 0;
            $fsizeSuffix = '';

            $fileInfo  = pathinfo($path);
            $fsizeInfo = $this->getFileSizeInfo($path);

            if ($fsizeInfo && $fsizeInfo['size']) {
                $fsize = $fsizeInfo['size'];
                $fsizeSuffix = $fsizeInfo['suffix'];
            }

            $fileObj = new Files();
            $fileObj->setCid($parentId);
            $fileObj->setFname($fileInfo['filename']);
            $fileObj->setFext($fileInfo['extension']);
            $fileObj->setFsize($fsize);
            $fileObj->setFsizeSuffix($fsizeSuffix);

            $this->files[] = $fileObj;

            return [true, true];
        }

        $dh = opendir($path);

        $nodeObj = null;
        if ($subpath) {
            $nodeObj = $this->tree->insertNewNode($subpath, $parentId);
            $parentId = $nodeObj->getId();
        }

        $filesCounter = 0;
        $type = 2; // default type = 2 - catalog with files
        while (false !== ($file = readdir($dh))) 
        {
            if($file != '.' && $file != '..') {

                $type = 1;
                list($result, $isFile) = $this->recursiveCreating($path.'/'.$file, $file, $parentId);

                if ($isFile) {
                    $filesCounter++;
                }
            }
        }
        closedir($dh);

        if (is_object($nodeObj)) {

            if ($filesCounter) {
                $type = 2;
                $nodeObj->setFcount($filesCounter);
            }
            $nodeObj->setType($type);
            $this->tree->catalogFlush($nodeObj);
        }

        return [true, false];
    }

    private function getFileSizeInfo($file)
    {
        if(!file_exists($file)) return false;

        $fileInfo = [
            'size'   => 0,
            'suffix' => ''
        ];

        $filesize = filesize($file);

        if($filesize > 1024) // > 1 kB
        {
            $filesize = ($filesize/1024);

            if($filesize > 1024) // > 1 MB
            {
                $filesize = ($filesize/1024);

                if($filesize > 1024) // > 1 GB
                {
                    $filesize = ($filesize/1024);
                    $fileInfo['size'] = round($filesize, 1);
                    $fileInfo['suffix'] = 'GB';
                    return $fileInfo;
                } else {
                    $fileInfo['size'] = round($filesize, 1);
                    $fileInfo['suffix'] = 'MB';
                    return $fileInfo;
                }
            } else {
                $fileInfo['size'] = round($filesize, 1);
                $fileInfo['suffix'] = 'kB';
                return $fileInfo;
            }  
        } else {
            $fileInfo['size'] = round($filesize, 1);
            $fileInfo['suffix'] = 'Byte';
            return $fileInfo;
        }
    }
}