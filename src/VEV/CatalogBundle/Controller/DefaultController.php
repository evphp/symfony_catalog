<?php

namespace VEV\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use VEV\CatalogBundle\Lib\Tree;

class DefaultController extends Controller
{
    public function indexAction()
    {
        // $em = $this->getDoctrine()->getManager();
       // $arrCatalog = $em->getRepository('VEVCatalogBundle:Catalog')->findAll();

        $doctrine = $this->getDoctrine();

        $tree = new Tree($doctrine);
        $catalogTree = $tree->getCatalog();

        $title = 'CATALOG';

        // type 1 - catalog
        // type 2 - folder (with files)
        $arrCatalog = [
            0 => [
                'id'     => 1,
                'title'  => 'Books',
                'type'   => 1,
                'fcount' => 0,
                'nodes'  => [
                    0 => [
                        'id'     => 3,
                        'title'  => 'Drama',
                        'type'   => 2,
                        'fcount' => 666,
                        'nodes'  => []
                    ],
                    1 => [
                        'id'     => 4,
                        'title'  => 'Fiction',
                        'type'   => 2,
                        'fcount' => 666,
                        'nodes'  => []
                    ],
                    2 => [
                        'id'     => 5,
                        'title'  => 'Fantasy',
                        'type'   => 2,
                        'fcount' => 666,
                        'nodes'  => []
                    ],
                    3 => [
                        'id'     => 6,
                        'title'  => 'Science',
                        'type'   => 1,
                        'fcount' => 0,
                        'nodes'  => [
                             0 => [
                                'id'     => 7,
                                'title'  => 'Theories',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            1 => [
                                'id'     => 8,
                                'title'  => 'Scientists',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            2 => [
                                'id'     => 9,
                                'title'  => 'Biology',
                                'type'   => 1,
                                'fcount' => 0,
                                'nodes'  => [
                                    0 => [
                                        'id'     => 10,
                                        'title'  => 'Evolution',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ],
                                    1 => [
                                        'id'     => 11,
                                        'title'  => 'Genetics',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ],
                                ]
                            ]
                        ]
                    ]// nodes Science
                ]
            ],
            1 => [
                'id'     => 2,
                'title'  => 'Articles',
                'type'   => 1,
                'fcount' => 0,
                'nodes'  => [
                    0 => [
                        'id'     => 12,
                        'title'  => 'Programming',
                        'type'   => 1,
                        'fcount' => 0,
                        'nodes'  => [
                            0 => [
                                'id'     => 13,
                                'title'  => 'C++',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            1 => [
                                'id'     => 14,
                                'title'  => 'PHP',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            2 => [
                                'id'     => 15,
                                'title'  => 'Frontend',
                                'type'   => 1,
                                'fcount' => 0,
                                'nodes'  => [
                                    1 => [
                                        'id'     => 16,
                                        'title'  => 'JS',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ],
                                    2 => [
                                        'id'     => 17,
                                        'title'  => 'CSS',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ],
                                    3 => [
                                        'id'     => 18,
                                        'title'  => 'HTML',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ]
                                ]
                            ]
                        ]
                    ],
                    1 => [
                        'id'     => 13,
                        'title'  => 'Auto',
                        'type'   => 1,
                        'fcount' => 0,
                        'nodes'  => [
                            0 => [
                                'id'     => 19,
                                'title'  => 'BMW',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            1 => [
                                'id'     => 20,
                                'title'  => 'Audi',
                                'type'   => 2,
                                'fcount' => 666,
                                'nodes'  => []
                            ],
                            2 => [
                                'id'     => 21,
                                'title'  => 'Sport',
                                'type'   => 1,
                                'fcount' => 0,
                                'nodes'  => [
                                    0 => [
                                        'id'     => 22,
                                        'title'  => 'Formula 1',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ],
                                    1 => [
                                        'id'     => 23,
                                        'title'  => 'Rally',
                                        'type'   => 2,
                                        'fcount' => 666,
                                        'nodes'  => []
                                    ]
                                ]
                            ]
                        ]
                    ]
                ] // nodes Articles
            ]
        ];

        return $this->render('VEVCatalogBundle:Catalog:index.html.twig', [
            'title' => $title,
            'catalogs' => $catalogTree
        ]);
    }
}
