<?php

namespace VEV\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    public function getFilesAction(Request $request)
    {
        $cid = $request->request->get('cid'); // get - query, post - request

        $response = array("success" => false);

        //handle data
        if ($cid) {
            $repository = $this->getDoctrine()->getRepository('VEVCatalogBundle:Files');
            $query = $repository->createQueryBuilder('f')
                ->where('f.cid = :cid')
                ->setParameter('cid', $cid)
                ->orderBy('f.fname','ASC')
                ->getQuery();

            $filesObj = $query->getResult();

            if (is_array($filesObj)) {

                $files = [];
                foreach ($filesObj as $obj) {
                    $files[] = [
                        'id'     => $obj->getId(),
                        'name'   => $obj->getFname(),
                        'ext'    => $obj->getFext(),
                        'size'   => $obj->getFsize(),
                        'suffix' => $obj->getFsizeSuffix(),
                    ];
                }
                if (count($files)) {
                    $response['success'] = true;
                    $response['files'] = $files;
                }
            }
        }

        return new Response(json_encode($response));
    }  
}