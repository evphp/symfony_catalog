<?php

namespace VEV\CatalogBundle\Command;

//use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use VEV\CatalogBundle\Console\CatalogConsole;

class CatalogCommand extends ContainerAwareCommand
{
    //protected $defaultOption;

    public function __construct() // $defaultOption
    {
        //$this->defaultOption = $defaultOption;
        parent::__construct();
    }

    protected function configure()
    {
        //$defaultOption = $this->defaultOption;

        $this
            ->setName('catalog:create')
            ->setDescription('Create recursive a new catalog with subdirectories and files.')
            ->addArgument('path', InputArgument::REQUIRED, 'Directory path.')
            // ->addOption(
            //     'name',
            //     '-n',
            //     InputOption::VALUE_REQUIRED,
            //     '', // default value
            // )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $inputPath = $input->getArgument('path');

        //$em = $this->getContainer()->get('doctrine')->getManager();
        $doctrine = $this->getContainer()->get('doctrine');

        $catalog = new CatalogConsole($doctrine);

        $rootDir = realpath(__DIR__. '/../../../../');
        $path = realpath($rootDir. '/'.$inputPath);

        $msg = 'Something wrong!!!';
        if (is_dir($path)) {
            $result = $catalog->create($path);
            if ($result) $msg = 'Catalog created successfully...';
        } else {
            $output->writeln('Path doesnt exitst: '.$rootDir.'\\'.$inputPath);
        }

        $output->writeln($msg);
    }
}