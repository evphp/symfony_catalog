<?php

namespace VEV\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Files
 *
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="VEV\CatalogBundle\Repository\FilesRepository")
 */
class Files
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *

     * @ORM\Column(type="integer")
     */
    private $cid;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $fname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    private $fext;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $fsize;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=4)
     */
    private $fsizeSuffix;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cid
     *
     * @param integer $cid
     *
     * @return Files
     */
    public function setCid($cid)
    {
        $this->cid = $cid;

        return $this;
    }

    /**
     * Get fsize
     *
     * @return int
     */
    public function getCid()
    {
        return $this->cid;
    }

    /**
     * Set fname
     *
     * @param string $fname
     *
     * @return Files
     */
    public function setFname($fname)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname
     *
     * @return string
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set fext
     *
     * @param string $fext
     *
     * @return Files
     */
    public function setFext($fext)
    {
        $this->fext = $fext;

        return $this;
    }

    /**
     * Get fext
     *
     * @return string
     */
    public function getFext()
    {
        return $this->fext;
    }

    /**
     * Set fsize
     *
     * @param integer $fsize
     *
     * @return Files
     */
    public function setFsize($fsize)
    {
        $this->fsize = $fsize;

        return $this;
    }

    /**
     * Get fsize
     *
     * @return int
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    /**
     * Set fsizeSuffix
     *
     * @param string $fsizeSuffix
     *
     * @return Files
     */
    public function setFsizeSuffix($fsizeSuffix)
    {
        $this->fsizeSuffix = $fsizeSuffix;

        return $this;
    }

    /**
     * Get fsizeSuffix
     *
     * @return string
     */
    public function getFsizeSuffix()
    {
        return $this->fsizeSuffix;
    }
}

