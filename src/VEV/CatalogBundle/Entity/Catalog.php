<?php

namespace VEV\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catalog
 *
 * @ORM\Table(name="catalog")
 * @ORM\Entity(repositoryClass="VEV\CatalogBundle\Repository\CatalogRepository")
 */
class Catalog
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $leftKey;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $rightKey;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $fcount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Catalog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set leftKey
     *
     * @param integer $leftKey
     *
     * @return Catalog
     */
    public function setLeftKey($leftKey)
    {
        $this->leftKey = $leftKey;

        return $this;
    }

    /**
     * Get leftKey
     *
     * @return int
     */
    public function getLeftKey()
    {
        return $this->leftKey;
    }

    /**
     * Set rightKey
     *
     * @param integer $rightKey
     *
     * @return Catalog
     */
    public function setRightKey($rightKey)
    {
        $this->rightKey = $rightKey;

        return $this;
    }

    /**
     * Get rightKey
     *
     * @return int
     */
    public function getRightKey()
    {
        return $this->rightKey;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Catalog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fcount
     *
     * @param integer $fcount
     *
     * @return Catalog
     */
    public function setFcount($fcount)
    {
        $this->fcount = $fcount;

        return $this;
    }

    /**
     * Get fcount
     *
     * @return int
     */
    public function getFcount()
    {
        return $this->fcount;
    }
}

