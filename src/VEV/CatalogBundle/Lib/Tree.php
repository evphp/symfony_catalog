<?php

namespace VEV\CatalogBundle\Lib;

use VEV\CatalogBundle\Entity\Catalog;

class Tree
{
    private $doctrine = null; // Doctrine object
    private $rawCatalog; // tmp var

    function __construct($doctrine = null) { 

        if (!$doctrine) {
            echo "Doctrine not defined...";
            exit();
        }
        $this->doctrine = $doctrine;
    }

    public function getCatalog()
    {
        $arrResult = [];
        $repository = $this->doctrine->getRepository('VEVCatalogBundle:Catalog');

        $query = $repository->createQueryBuilder('c')
            // ->andWhere('c.id IN (:id)')
            // ->setParameter('id', array(1,2,11)))
            ->orderBy('c.leftKey','ASC')
            ->getQuery();
        $this->rawCatalog = $query->getResult(); // getSingleResult(); getOneOrNullResult();

        list($arrResult) = $this->buildTree();

        return $arrResult;
    }

    /*
     * recursive function 
     */
    private function buildTree($i = 0, $parentRightKey = 0)
    {
        $arrTree = [];
        $is_end_node = false;

        while ($i < count($this->rawCatalog) && !$is_end_node) {

            $obj = $this->rawCatalog[$i];

            $id = $obj->getId();
            $leftKey = $obj->getLeftKey();
            $rightKey = $obj->getRightKey();

            $i++;

            $nodes = [];

            // has child nodes
            if ($leftKey + 1 < $rightKey) {
                list($nodes, $i) = $this->buildTree($i, $rightKey);
            }

            $newNode = [
                'id'     => $obj->getId(),
                'title'  => $obj->getTitle(),
                'type'   => $obj->getType(),
                'fcount' => $obj->getFcount(),
                'nodes'  => $nodes
            ];

            if ($newNode['type'] == 2) { // for nice view in tree
                array_unshift($arrTree, $newNode);
            } else {
                $arrTree[] = $newNode;
            }

            $is_end_node = ($parentRightKey != 0 && ($rightKey + 1 == $parentRightKey)) ? true : false;
        }

        return [$arrTree, $i];
    }

    public function insertNewNode($name, $parentId = 0)
    {
        $catalog = null;
        $em = $this->doctrine->getManager();

        $type     = 2;
        $leftKey  = 0; // ordering - top | bottom
        $rightKey = 0; // inheritance - uses as leftKey for child nodes


        if (!$parentId) { // parent id not defined
            $leftKey  = 1; // for first record
            $rightKey = 2; 

            $repository = $this->doctrine->getRepository('VEVCatalogBundle:Catalog');
            $query = $repository->createQueryBuilder('c')
                ->select('MAX(c.rightKey) AS max_right_key')
                ->getQuery();
            $queryResult = $query->getSingleResult();

            if ($queryResult['max_right_key']) {
                $leftKey = $queryResult['max_right_key'] + 1;
                $rightKey = $leftKey + 1;
            }

        } else {

            $parentNode = $this->doctrine->getRepository('VEVCatalogBundle:Catalog')->find($parentId);
            $em->refresh($parentNode);

            $paretLeftKey   = $parentNode->getLeftKey();
            $parentRightKey = $parentNode->getRightKey();

            // add to bottom list
            $leftKey = $parentRightKey; 
            $rightKey = $leftKey + 1;

            $this->updateKeys($leftKey);
        }

        $catalog = new Catalog();
        $catalog->setTitle($name);
        $catalog->setType($type);
        $catalog->setLeftKey($leftKey);
        $catalog->setRightKey($rightKey);
        $catalog->setFcount(0);

        $em->persist($catalog);
        $em->flush();

        return $catalog;
    }

    public function catalogFlush($catalog)
    {
        if (is_object($catalog)) { 
            $em = $this->doctrine->getManager();
            $em->persist($catalog);
            $em->flush();
        }
        return $catalog;
    }

    /*
     * update tree keys to create empty intreval for new node
     * warning! maybe will need to refresh entity manager (->refresh($entity))
     */
    private function updateKeys($key)
    {
        $repository = $this->doctrine->getRepository('VEVCatalogBundle:Catalog');

        $query = $repository->createQueryBuilder('c')
            ->update()
            ->set('c.rightKey', 'c.rightKey + 2')
            ->set('c.leftKey', 'case when c.leftKey >= :key then c.leftKey + 2 else c.leftKey end')
            ->where('c.rightKey >= :key')
            ->setParameter('key', $key)
            ->getQuery();
        $res = $query->execute();

        return $res;
    }

}